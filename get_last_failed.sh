#!/bin/bash
if [[ -z $1 || -z $2 ]]; then
        echo "Script usage: get_last_failed.sh PRIVATE_TOKEN PROJECT_ID"
        echo "Example: get_last_failed.sh asdfg789yq240g7ya07 27542426"
else
        curl -# --header "PRIVATE-TOKEN: $1" "https://gitlab.com/api/v4/projects/$2/jobs" | jq '[.[] | select (.status == "failed" and .name == "build_prod")][0]'
fi
